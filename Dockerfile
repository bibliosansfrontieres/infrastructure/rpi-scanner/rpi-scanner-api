###############
# BUILD STAGE #
###############

FROM node:20-alpine AS build

# Install dev node modules in /app
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --omit=dev

# Install production node modules in /build
WORKDIR /build
COPY package.json package-lock.json ./
RUN npm install

# Build project in /build
COPY . .
RUN npm run build

####################
# PRODUCTION STAGE #
####################

FROM node:20-alpine AS production

WORKDIR /app

RUN apk update && apk add arp-scan iputils bind-tools

# Copy node modules from /app
COPY --from=build /app/node_modules /app/node_modules

# Copy builded node project
COPY --from=build /build/dist /app/dist
COPY --from=build /build/data /app/data
COPY package.json /app

ENV NODE_ENV production

CMD [ "node", "dist/main.js" ]
