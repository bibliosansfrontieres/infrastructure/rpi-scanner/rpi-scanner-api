# RPi Scanner API

This project is used to scan local network to get hosts connected.

Will ping every hosts that are found to check if they are alive.

Will get every hosts alive hostnames.

Will check if every RPi alive are OLIP based, if they are deployed, etc...

## Use for development

Run :

```
docker compose up
```
