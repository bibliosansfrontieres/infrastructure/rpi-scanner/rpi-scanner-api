import { Module } from '@nestjs/common';
import { ScannerModule } from './scanner/scanner.module';
import { RpisModule } from './rpis/rpis.module';
import { ConfigModule } from '@nestjs/config';
import { schema } from './config/schema';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rpi } from './rpis/entities/rpi.entity';
import { OlipCheckerModule } from './olip-checker/olip-checker.module';
import { SshModule } from './ssh/ssh.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'data/database.db',
      entities: [Rpi],
      synchronize: true,
    }),
    ConfigModule.forRoot({
      validationSchema: schema,
    }),
    ScannerModule,
    RpisModule,
    OlipCheckerModule,
    SshModule,
  ],
})
export class AppModule {}
