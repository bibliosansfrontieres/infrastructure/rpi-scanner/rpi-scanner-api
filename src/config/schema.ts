import * as Joi from 'joi';

export const schema = Joi.object({
  API_SERVER_PORT: Joi.number().default(80),
  SLEEP_BETWEEN_NETWORK_SCAN: Joi.number().default(10000),
  SLEEP_BETWEEN_CHECKS: Joi.number().default(10000),
  PING_TIMEOUT: Joi.number().default(5000),
  PING_THREADS: Joi.number().default(5),
  GETTING_HOSTNAMES_TIMEOUT: Joi.number().default(5000),
  GETTING_HOSTNAMES_THREADS: Joi.number().default(5),
});
