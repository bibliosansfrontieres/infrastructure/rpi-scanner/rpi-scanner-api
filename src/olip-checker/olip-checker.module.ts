import { Module } from '@nestjs/common';
import { OlipCheckerService } from './olip-checker.service';
import { RpisModule } from 'src/rpis/rpis.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule, RpisModule],
  providers: [OlipCheckerService],
})
export class OlipCheckerModule {}
