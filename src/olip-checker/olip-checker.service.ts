import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RpisService } from 'src/rpis/rpis.service';
import { SoftwareEnum } from 'src/utils/enums/software.enum';
import { isOlipV1 } from 'src/utils/is-olip-v1.util';
import { sleep } from 'src/utils/sleep.util';

@Injectable()
export class OlipCheckerService {
  private sleepBetweenChecks: number;

  constructor(
    configService: ConfigService,
    private rpisService: RpisService,
  ) {
    this.sleepBetweenChecks = configService.get('SLEEP_BETWEEN_CHECKS');
  }

  onModuleInit() {
    this.checkRpis();
  }

  private async checkRpis() {
    Logger.log('Checking Raspberry Pis...', 'OlipCheckerService');
    await this.checkIsOlip();
    await this.checkSerial();
    await this.checkDeployed();
    Logger.log('Raspberry Pis check finished !', 'OlipCheckerService');
    Logger.log(
      `Sleeping ${this.sleepBetweenChecks}ms before next check...`,
      'OlipCheckerService',
    );
    await sleep(this.sleepBetweenChecks);
    this.checkRpis();
  }

  private async checkIsOlip() {
    const aliveRpis = await this.rpisService.find({ where: { isAlive: true } });
    for (const aliveRpi of aliveRpis) {
      if (isOlipV1(aliveRpi) && aliveRpi.software !== SoftwareEnum.OLIP_V1) {
        aliveRpi.software = SoftwareEnum.OLIP_V1;
        await this.rpisService.save(aliveRpi);
      }
    }
  }

  private async checkSerial() {
    const olipRpis = await this.rpisService.find({
      where: [{ software: SoftwareEnum.OLIP_V1, isAlive: true }],
    });

    for (const olipRpi of olipRpis) {
      await this.rpisService.getSerial(olipRpi);
    }
  }

  private async checkDeployed() {
    const olipRpis = await this.rpisService.find({
      where: [{ software: SoftwareEnum.OLIP_V1, isAlive: true }],
    });

    for (const olipRpi of olipRpis) {
      await this.rpisService.getIsDeployed(olipRpi);
    }
  }
}
