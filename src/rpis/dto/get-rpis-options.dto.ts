import { IsEnum, IsOptional } from 'class-validator';
import { SoftwareEnum } from 'src/utils/enums/software.enum';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';

export class GetRpisOptionsDto {
  @IsOptional()
  @IsEnum(TrueFalseEnum)
  isAlive?: TrueFalseEnum;

  @IsOptional()
  @IsEnum(SoftwareEnum)
  software?: SoftwareEnum;
}
