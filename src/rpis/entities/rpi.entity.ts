import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IDevice } from '@bibliosansfrontieres/network-arp-scan';
import { SoftwareEnum } from 'src/utils/enums/software.enum';

@Entity()
export class Rpi {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ip: string;

  @Column()
  macAddress: string;

  @Column()
  isAlive: boolean;

  @Column()
  software: SoftwareEnum;

  @Column()
  isDeployed: boolean;

  @Column({ nullable: true })
  name: string | null;

  @Column({ nullable: true })
  hostname: string | null;

  @Column({ nullable: true })
  serial: string | null;

  constructor(device: IDevice) {
    if (!device) return;
    this.ip = device.ip;
    this.macAddress = device.macAddress;
    this.name = device.name;
    this.isAlive = device.isAlive;
    this.hostname = device.hostname;
    this.software = SoftwareEnum.UNDEFINED;
    this.isDeployed = false;
  }
}
