import { Controller, Get, Param, Query } from '@nestjs/common';
import { RpisService } from './rpis.service';
import { GetRpisOptionsDto } from './dto/get-rpis-options.dto';

@Controller('rpis')
export class RpisController {
  constructor(private readonly rpisService: RpisService) {}

  @Get()
  getRpis(@Query() getRpisOptionsDto: GetRpisOptionsDto) {
    return this.rpisService.getRpis(getRpisOptionsDto);
  }

  @Get('blink/:id')
  getBlink(@Param('id') id: string) {
    return this.rpisService.getBlink(+id);
  }

  @Get('deploy/:id')
  getDeploy(@Param('id') id: string) {
    return this.rpisService.getDeploy(+id);
  }

  @Get('shutdown/:id')
  getShutdown(@Param('id') id: string) {
    return this.rpisService.getShutdown(+id);
  }

  @Get('reboot/:id')
  getReboot(@Param('id') id: string) {
    return this.rpisService.getReboot(+id);
  }
}
