import { Module } from '@nestjs/common';
import { RpisService } from './rpis.service';
import { RpisController } from './rpis.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rpi } from './entities/rpi.entity';
import { SshModule } from 'src/ssh/ssh.module';

@Module({
  imports: [TypeOrmModule.forFeature([Rpi]), SshModule],
  controllers: [RpisController],
  providers: [RpisService],
  exports: [RpisService],
})
export class RpisModule {}
