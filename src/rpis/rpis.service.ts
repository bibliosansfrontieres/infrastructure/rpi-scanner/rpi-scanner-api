import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';
import { IDevice } from '@bibliosansfrontieres/network-arp-scan';

import { Rpi } from './entities/rpi.entity';
import { isRpiMacAddress } from 'src/utils/is-rpi-mac-address.util';
import { GetRpisOptionsDto } from './dto/get-rpis-options.dto';
import { TrueFalseEnum } from 'src/utils/enums/true-false.enum';
import { SshService } from 'src/ssh/ssh.service';
import { SoftwareEnum } from 'src/utils/enums/software.enum';

@Injectable()
export class RpisService {
  constructor(
    @InjectRepository(Rpi) private rpisRepository: Repository<Rpi>,
    private sshService: SshService,
  ) {}

  async getRpis(getRpisOptionsDto: GetRpisOptionsDto) {
    const where: FindOptionsWhere<Rpi> = {};

    if (getRpisOptionsDto?.isAlive === TrueFalseEnum.TRUE) {
      where.isAlive = true;
    }

    if (getRpisOptionsDto?.software) {
      where.software = getRpisOptionsDto.software;
    }

    const rpis = await this.find({ where });
    return rpis;
  }

  async updateRpis(devices: IDevice[]) {
    devices = this.filterRpis(devices);

    for (const device of devices) {
      const rpi = await this.findOne({
        where: [{ ip: device.ip }, { macAddress: device.macAddress }],
      });
      if (!rpi) {
        await this.create(device);
      } else {
        await this.update(rpi, device);
      }
    }

    const rpis = await this.find();
    for (const rpi of rpis) {
      const device = devices.find(
        (d) => d.ip === rpi.ip || d.macAddress === rpi.macAddress,
      );
      if (!device) {
        await this.update(rpi);
      }
    }
  }

  async getSerial(rpi: Rpi) {
    const command = 'fgrep Serial /proc/cpuinfo';
    const result = await this.sshService.sendCommand(rpi, command);
    if (result !== undefined && result.includes('Serial')) {
      const serial = result.split('Serial')[1].split(':')[1].trim();
      rpi.serial = serial;
      await this.save(rpi);
    }
  }

  async getIsDeployed(rpi: Rpi) {
    if (rpi.software === SoftwareEnum.OLIP_V1) {
      const command =
        '[ -f "/opt/olip-deploy" ] && echo "true" || echo "false"';
      const result = await this.sshService.sendCommand(rpi, command);
      if (result !== undefined) {
        if (result.includes('true') || result.includes('false')) {
          const isDeployed = result.trim() === 'true';
          rpi.isDeployed = isDeployed;
          await this.save(rpi);
        }
      }
    }
  }

  async getBlink(id: number) {
    const rpi = await this.findOne({ where: { id } });
    if (!rpi) return;
    const command =
      'sudo systemctl stop trigger_ideascube_led.service && sudo make-my-cube-blink.sh crazy 100 && sudo systemctl start trigger_ideascube_led.service';
    await this.sshService.sendCommand(rpi, command);
  }

  async getDeploy(id: number) {
    const rpi = await this.findOne({ where: { id } });
    if (!rpi) return;
    const command = `sudo crontab < <(sudo crontab -l | sed '/^#/s/^#//') && sudo nohup deploy.py > /dev/null 2>&1 &`;
    await this.sshService.sendCommand(rpi, command);
  }

  async getShutdown(id: number) {
    const rpi = await this.findOne({ where: { id } });
    if (!rpi) return;
    const command = `sudo shutdown -h now`;
    await this.sshService.sendCommand(rpi, command);
  }

  async getReboot(id: number) {
    const rpi = await this.findOne({ where: { id } });
    if (!rpi) return;
    const command = `sudo reboot`;
    await this.sshService.sendCommand(rpi, command);
  }

  findOne(options?: FindOneOptions<Rpi>) {
    return this.rpisRepository.findOne(options);
  }

  find(options?: FindManyOptions<Rpi>) {
    return this.rpisRepository.find(options);
  }

  save(rpi: Rpi) {
    return this.rpisRepository.save(rpi);
  }

  private update(rpi: Rpi, device?: IDevice) {
    if (!device) {
      rpi.isAlive = false;
      return this.save(rpi);
    }

    rpi.ip = device.ip;
    rpi.macAddress = rpi.macAddress;
    rpi.name = device.name;
    rpi.hostname = device.hostname;
    rpi.isAlive = device.isAlive;

    return this.save(rpi);
  }

  private async create(device: IDevice) {
    const rpi = new Rpi(device);
    return this.save(rpi);
  }

  private filterRpis(devices: IDevice[]) {
    return devices.filter((device) => {
      return isRpiMacAddress(device.macAddress);
    });
  }
}
