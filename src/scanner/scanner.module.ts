import { Module } from '@nestjs/common';
import { ScannerService } from './scanner.service';
import { ConfigModule } from '@nestjs/config';
import { RpisModule } from 'src/rpis/rpis.module';

@Module({
  imports: [ConfigModule, RpisModule],
  providers: [ScannerService],
})
export class ScannerModule {}
