import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NetworkScanner } from '@bibliosansfrontieres/network-arp-scan';
import { RpisService } from 'src/rpis/rpis.service';
import { sleep } from 'src/utils/sleep.util';

@Injectable()
export class ScannerService {
  private sleepBetweenNetworkScan: number;
  private networkScanner: NetworkScanner;

  constructor(
    private configService: ConfigService,
    private rpisService: RpisService,
  ) {
    this.sleepBetweenNetworkScan = this.configService.get(
      'SLEEP_BETWEEN_NETWORK_SCAN',
    );
    this.networkScanner = new NetworkScanner({
      pingingDevices: true,
      pingTimeout: 5000,
      pingThreads: 5,
      gettingHostnames: true,
      gettingHostnamesThreads: 5,
      gettingHostnamesTimeout: 5000,
    });
  }

  onModuleInit() {
    this.scan();
  }

  async scan() {
    Logger.log('Running network scanner...', 'ScannerService');
    const devices = await this.networkScanner.scanNetwork();
    Logger.log('Network scanner finished !', 'ScannerService');
    Logger.log('Updating Raspberry Pis...', 'ScannerService');
    await this.rpisService.updateRpis(devices);
    Logger.log('Raspberry Pis updated !', 'ScannerService');
    Logger.log(
      `Sleeping ${this.sleepBetweenNetworkScan}ms before next network scan...`,
      'ScannerService',
    );
    await sleep(this.sleepBetweenNetworkScan);
    this.scan();
  }
}
