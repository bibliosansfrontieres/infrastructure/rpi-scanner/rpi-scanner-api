export interface ISshConfig {
  host: string;
  username: string;
  password: string;
}
