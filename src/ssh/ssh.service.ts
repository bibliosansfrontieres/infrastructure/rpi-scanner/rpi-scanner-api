import { Injectable } from '@nestjs/common';
import { Rpi } from 'src/rpis/entities/rpi.entity';
import { ssh } from 'src/utils/ssh.util';

@Injectable()
export class SshService {
  async sendCommand(rpi: Rpi, command: string) {
    const sshConfig = {
      host: rpi.ip,
      port: 22,
      username: 'ideascube',
      password: '5bfae106af',
      readyTimeout: 2000,
    };

    let result = await ssh(sshConfig, command);
    if (result) return result;

    sshConfig.password = 'ideascube';
    result = await ssh(sshConfig, command);
    return result;
  }
}
