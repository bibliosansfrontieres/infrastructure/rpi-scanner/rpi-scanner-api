import { Rpi } from 'src/rpis/entities/rpi.entity';

export function isOlipV1(rpi: Rpi) {
  if (!rpi.hostname) return false;
  return (
    rpi.hostname.startsWith('ideascube') || rpi.hostname.startsWith('idc-')
  );
}
