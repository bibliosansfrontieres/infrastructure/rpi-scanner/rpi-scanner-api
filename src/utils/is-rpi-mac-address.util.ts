const rpiMacAddressesStarting = ['e4:5f:01', 'dc:a6:32'];

export function isRpiMacAddress(macAddress: string) {
  let isRpiMacAddress = false;
  for (const rpiMacAddressStarting of rpiMacAddressesStarting) {
    if (macAddress.toLowerCase().startsWith(rpiMacAddressStarting)) {
      isRpiMacAddress = true;
      break;
    }
  }
  return isRpiMacAddress;
}
