import { ISshConfig } from 'src/ssh/interfaces/ssh-config.interface';
import { Client } from 'ssh2';

export async function ssh(sshConfig: ISshConfig, command: string) {
  try {
    return new Promise<string>((resolve, reject) => {
      const ssh = new Client();

      ssh.on('ready', () => {
        ssh.exec(command, (err, stream) => {
          if (err) {
            ssh.end();
            reject(err);
            return;
          }

          let commandOutput = '';

          stream
            .on('data', (data: Buffer) => {
              commandOutput += data.toString();
            })
            .on('close', (code: number, err: any) => {
              ssh.end();
              if (code === 0) {
                resolve(commandOutput);
              } else {
                reject(err);
              }
            });
        });
      });

      ssh.on('error', (err) => {
        reject(err);
      });

      ssh.connect(sshConfig);
    });
  } catch (error) {
    return undefined;
  }
}
